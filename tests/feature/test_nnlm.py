import time

import pytest
import pandas as pd

from grabds.feature.nnlm import NNLM


def test_nnlm():
    types = ['nnlm50', 'nnlm128']
    test_data = [
        "hihi",
        "hello"
    ]
    for t in types:
        feat = NNLM(t)
        out = feat.infer(test_data)
        assert out.shape == (2, feat.output_dim)
