import time

import pytest
import pandas as pd

from grabds.data.pandas import PandasFeatureStore


@pytest.fixture(scope='function')
def store():
    return PandasFeatureStore()


def test_pandas_merge(store):
    assert store.max_index() == -1

    test_data = pd.DataFrame({
        'a': [1, 2, 3],
        'b': [2, 3, 4],
    }, index=[0, 1, 2])
    store.merge(test_data)
    print(store.df)
    assert store.max_index() == 2

    test_data = pd.DataFrame({
        'a': [1, 2, 3],
        'b': [2, 3, 4],
    }, index=[3, 4, 5])

    store.merge(test_data)
    print(store.df)
    assert store.max_index() == 5


def test_pandas_save(store):
    test_data = pd.DataFrame({
        'a': [1, 2, 3],
        'b': [2, 3, 4],
    }, index=[0, 1, 2])
    store.merge(test_data)
    print(store.df)
    assert store.max_index() == 2

    test_path = f'test-pandas-{time.time()}.pkl'
    store.save(test_path)

    store2 = PandasFeatureStore(test_path)
    assert (store2.df.values == store.df.values).all()
