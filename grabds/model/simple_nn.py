from typing import List

import tensorflow.keras as keras
import numpy as np


class SimpleNNModel:
    model: keras.Model
    hidden_nodes: List[int]
    input_dim: int

    def __init__(self, hidden_nodes: List[int] = [32, 16], input_dim=50, model=None, **kwargs):
        if len(hidden_nodes) <= 1:
            raise ValueError('there must be at least 1 hidden layer')
        self.hidden_nodes = hidden_nodes
        self.input_dim = input_dim

        if model:
            self.model = model
        else:
            model = keras.Sequential()
            for i, n in enumerate(hidden_nodes):
                if i == 0:
                    model.add(keras.layers.Dense(n, activation='relu',
                                                 input_shape=(input_dim,)))
                else:
                    model.add(keras.layers.Dense(n, activation='relu'))

            model.add(keras.layers.Dense(3, activation=None))
            model.compile(optimizer='sgd',
                          loss=keras.losses.CategoricalCrossentropy(),
                          metrics=['accuracy'])
            self.model = model

    def get_metadata(self):
        return {
            'type': 'simple_nn',
            'hidden_nodes': self.hidden_nodes,
            'input_dim': self.input_dim,
        }

    @staticmethod
    def load_model(metadata, path):
        model = keras.models.load_model(path)
        return SimpleNNModel(model=model, **metadata)

    def summary(self):
        self.model.summary()

    def save_model(self, path):
        self.model.save(path)

    def keras_layer(self):
        return self.model

    def infer(self, s):
        return np.argmax(self.model.predict(s), axis=1)
