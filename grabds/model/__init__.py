from .simple_nn import SimpleNNModel

models = {
    'simple_nn': SimpleNNModel,
}


def get_model_cls(model_type):
    if model_type not in models:
        raise ValueError(f'unrecognized model type: {model_type}')
    return models[model_type]


def load_model(metadata, path):
    model_type = metadata['type']
    if model_type not in models:
        raise ValueError(f'unrecognized model type: {model_type}')
    return models[model_type].load_model(metadata, path)
