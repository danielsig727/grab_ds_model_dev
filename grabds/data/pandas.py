import os
import pandas as pd


class PandasFeatureStore:
    _df: pd.DataFrame

    def __init__(self, load_pickle: str = None):
        """Get a new pandas feature store

        Args:
            load_pickle(str): load pickled data from path
        """

        if load_pickle and os.path.exists(load_pickle):
            self._df = pd.read_pickle(load_pickle)
        else:
            self._df = pd.DataFrame()

    def save(self, path):
        self._df.to_pickle(path)

    def max_index(self) -> int:
        if len(self._df) == 0:
            return -1
        return max(self._df.index)

    def merge(self, df):
        self._df = pd.concat([self._df, df], axis=0)

    @property
    def df(self):
        return self._df
