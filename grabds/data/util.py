import pandas as pd
import sqlite3
import re
import random


def load_sqlite_dataset(db_file_path, table):
    """
    Load everything from a sqlite3 table
    """
    con = sqlite3.connect(db_file_path)

    table_data = pd.read_sql_query(f'select * from `{table}`', con)

    con.close()
    return table_data


def remove_symbols(s):
    """
    Remove symbols from string
    """
    return re.sub(r'[^\w]', ' ', s)


def assign_train_test(data, test_ratio=0.15, column='dataset'):
    """
    Add a new column in DataFrame and
    randomly assign `testing` or `training` according to the ratio.
    """
    num_testing = int(len(data) * test_ratio)
    num_training = len(data) - num_testing
    sets = [*(['training'] * num_training), *(['testing'] * num_testing)]
    random.shuffle(sets)
    data[column] = sets
    return (num_training, num_testing)
