import yaml
import logging

import tensorflow.keras as keras

from grabds.feature import get_feature
from grabds.model import load_model


def pack_model_tf(metadatapath, modelpath, savepath):
    """
    Create a tf SavedModel combining feature and trained model

    Args:
        metadatapath: (str) The path of model metadata yaml file.
        modelpath: (str) The path of model weights.
        savepath: (str) The path to save the packed tf SavedModel to.
    """
    with open(metadatapath, 'r') as f:
        metadata = yaml.load(f)

    logging.info(f'loaded metadata: {metadata}')

    logging.info('loading feature')
    feature = get_feature(metadata['feature'])
    logging.info('loading model')
    model = load_model(metadata['model'], modelpath)

    logging.info('merging model')
    merged_model = keras.Sequential()
    merged_model.add(feature.keras_layer())
    merged_model.add(model.keras_layer())
    merged_model.add(keras.layers.Softmax())
    merged_model.compile(optimizer='adam', loss=keras.losses.CategoricalCrossentropy(), metrics=['accuracy'])

    merged_model.summary()

    logging.info('saving model')
    merged_model.save(savepath)


def cli():
    import coloredlogs

    coloredlogs.install(level='DEBUG')

    from fire import Fire
    Fire(pack_model_tf)


if __name__ == '__main__':
    cli()
