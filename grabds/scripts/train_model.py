import logging
import os
import yaml

import tensorflow.keras as keras
import numpy as np

from grabds.data.pandas import PandasFeatureStore
from grabds.model import load_model, get_model_cls


logger = logging.getLogger('train_model')


def train_model(metadata_path,
                model_path,
                model_type='simple_nn',
                store_path='feature_pd.pkl',
                val_split=0.15,
                num_epoch=50,
                feature='nnlm128',
                feature_size=128,
                hidden_nodes=[32, 32]):
    """
    Train a model using dataset from feature store.
    If files at metadata_path and model_path already exists, the script will
    attempt to load the model from the path and finetune them.

    Args:
        metadata_path: (str) The metadata yaml to load from and save to.
        model_path: (str) The model path to load from and save to.
        model_type: (str) The model type to train (default: simple_nn)
        store_path: (str) Path of pandas feature store (default: feature_pd.pkl)
        val_split: (float) Ratio of data for validation during training (default: 0.15)
        num_epoch: (int) Number of epochs to train (default: 50)
        feature: (str) The type of feature to as model input (default: nnlm128)
        feature_size: (int) The size of feature (default: 128)
        hidden_nodes: (list[int]) # of nodes of each hidden layer (default: [32, 32])
    """
    if os.path.exists(metadata_path):
        with open(metadata_path, 'r') as f:
            metadata = yaml.load(f)
        model = load_model(metadata['model'], model_path)
    else:
        model = get_model_cls(model_type)(input_dim=feature_size, hidden_nodes=hidden_nodes)
    model.summary()

    store = PandasFeatureStore(store_path)
    train_data = store.df[store.df.dataset == 'training']
    test_data = store.df[store.df.dataset == 'testing']
    logging.info(f'loaded {len(train_data)} training samples and {len(test_data)} testing samples')

    logger.info('training model')
    model.model.fit(np.vstack(train_data[f'feature-{feature}']),
                    keras.utils.to_categorical(np.vstack(train_data[f'class_label']),
                                               num_classes=3),
                    validation_split=val_split, shuffle=True, batch_size=64, epochs=num_epoch)

    logger.info('evaluating model')
    model.model.evaluate(np.vstack(test_data[f'feature-{feature}']),
                         keras.utils.to_categorical(np.vstack(test_data[f'class_label']),
                                                    num_classes=3),
                         batch_size=64)

    logger.info('saving model and metadata')
    metadata = {
        'model': model.get_metadata(),
        'feature': feature,
    }
    with open(metadata_path, 'w') as f:
        yaml.dump(metadata, f)
    model.save_model(model_path)


def cli():
    import coloredlogs

    coloredlogs.install(level='DEBUG')

    from fire import Fire
    Fire(train_model)


if __name__ == '__main__':
    cli()
