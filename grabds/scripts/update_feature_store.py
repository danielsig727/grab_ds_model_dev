import logging

from grabds.data.pandas import PandasFeatureStore
from grabds.data.util import load_sqlite_dataset, remove_symbols, assign_train_test
from grabds.feature import get_feature

logger = logging.getLogger('update_feature_store')


def cleanup_data(df, preprocessings):
    for _, row in df.iterrows():
        for f in preprocessings:
            row['text'] = f(row['text'])


def update_feature_store(db_path,
                         store_path='feature_pd.pkl',
                         test_ratio=0.15,
                         features=['nnlm50', 'nnlm128']):
    """
    Import data into feature store.
    After invoking, data from the given sqlite database and extracted features will be
    imported into the given feature with assignments into testing or training sets.

    Args:
        db_path: (str) The path of sqlite3 db file containing raw data
        store_path: (str) The path of pandas data store (default: feature_pd.pkl)
        test_ratio: (float) The ratio of data to be assigned as testing set. The rest is training set. (default: 0.15)
        features: (list[str]) The feature(s) to be generated and stored into store (default: [nnlm50, nnlm128])
    """
    preprocessings = [remove_symbols]
    store = PandasFeatureStore(store_path)

    logging.info('loading data from sqlite')
    raw_data = load_sqlite_dataset(db_path, 'data')
    logging.info(f'=> loaded {len(raw_data)} entries')

    # assuming the index is monotonic increasing.
    # use it to compare against existing data in the store
    logging.info('removing existing data')
    max_idx = store.max_index()
    raw_data = raw_data[raw_data.index > max_idx]
    logging.info(f'=> {len(raw_data)} entries after deduplication')

    logging.info('preprocessing data')
    cleanup_data(raw_data, preprocessings)

    logging.info('assinging data sets')
    num_training, num_testing = assign_train_test(raw_data, test_ratio)
    logging.info(f"=> assigned {num_training} training samples and {num_testing} testing samples")

    logging.info('generating features')
    for fname in features:
        logging.info(f'=> processing feature: {fname}')
        feat = get_feature(fname)
        feat_vec = feat.infer(raw_data['text'])
        raw_data[f'feature-{fname}'] = feat_vec.tolist()

    print(raw_data)

    store.merge(raw_data)

    print(store.df)

    store.save(store_path)


def cli():
    import coloredlogs

    coloredlogs.install(level='DEBUG')

    from fire import Fire
    Fire(update_feature_store)


if __name__ == '__main__':
    cli()
