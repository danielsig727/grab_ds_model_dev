from typing import List

import tensorflow as tf
import tensorflow_hub as hub


nnlm_urls = {
    'nnlm50': ('https://tfhub.dev/google/nnlm-en-dim50/2', 50),
    'nnlm128': ('https://tfhub.dev/google/nnlm-en-dim128-with-normalization/2', 128),
}


class NNLM:
    url: str
    model: hub.Module
    _output_dim: int

    def __init__(self, nnlm_type):
        self.url, self._output_dim = nnlm_urls[nnlm_type]
        self.model = hub.load(self.url)

    def infer(self, s: List[str]):
        return self.model(s).numpy()

    @property
    def output_dim(self):
        return self._output_dim

    def keras_layer(self):
        return hub.KerasLayer(self.url,
                              input_shape=[], dtype=tf.string)


class NNLM50(NNLM):
    def __init__(self):
        super().__init__('nnlm50')


class NNLM128(NNLM):
    def __init__(self):
        super().__init__('nnlm128')
