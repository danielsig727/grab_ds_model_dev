from .nnlm import NNLM50, NNLM128

features = {
    'nnlm50': NNLM50,
    'nnlm128': NNLM128,
}


def get_feature(name: str):
    if name not in features:
        raise NameError(f'unknown feature {name}')
    return features[name]()
