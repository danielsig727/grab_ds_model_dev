#!/bin/bash -xe

echo test importing the small data set
grabds_update_feature_store ./ci/db_small.sqlite ./ci/store.pkl

echo test training the 1st model
grabds_train_model --store_path=./ci/store.pkl --num_epoch=10 ci/tmp_model.yaml ci/tmp_model.h5

echo test importing the larger data set
grabds_update_feature_store ./ci/db_large.sqlite ./ci/store.pkl

echo test funetuning the pretrained model
grabds_train_model --store_path=./ci/store.pkl --num_epoch=10 ci/tmp_model.yaml ci/tmp_model.h5
