# Grab DS Model Dev

This repository is a MVP of a model development flow.

Author: Shao-Heng Tai [danielsig727@gmail.com](mailto:danielsig727@gmail.com)

## Workflow design

The main working flow is divided into several flows:

### 1. Data importing

Including 

1. Loading raw data from external source (a sqlite3 database in this case)
2. Deduplication, remove existing data from loaded raw data
3. Cleaning, such as removing symbols from sentences
4. Dataset assigning. Randomly assigning each data in training set or training set at specified ratio
5. Feature extracting. Transforming the data into several kinds of features
6. Feature storing. Storing the data into a feature store.

### 2. Model training

Train the model using data from feature store and perform evaluation. Finally, save the model.

### 3. Model packing

Pack the feature extractor and trained model as a merged model. The merged model can be imported into the other project `grab_ds_model_serving` for serving.

## Project Design

The repo consists of a python package `grabds`.

There are several sub-packages in `grabds`. 

- `data` contains data-related tools. `data.pandas` is a minimal implementation of a feature store based on pandas. `data.util` contains helper functions of loading data from a given sqlite3 database.
- `feature` implements several feature extractors with common interface to different feature extractors.
- `model` includes model object implementation
- `script` is the implementation of the 3 flows above. With each of the flow can be called using command-line interface, or be called from other python code.

The idea is to generalize the access interface of data, feature, and model objects. So they can be used in different combinations in the flows.

### Model serialization

Each trained model is saved into 2 files - metadata yaml file and model binary file. The metadata file contains information about the training process, currently feature type and model's hyper-parameters. The model binary file contains the weights of the model. The format of the binary file can be specific to each model type, but a general load function in each model must be provided as the interface to the general loader.

In general, the design allows the model metadata to be archived in databases. And the binary files, usually much larger, can be saved to object storages.

## Usage

### Installation

[Poetry](https://python-poetry.org/) is used as the package manager to provide better maintainability and stability. To install the package first make sure poetry is installed

    pip install poetry==1.0.0 

Poetry is capable of creating virtual environments automatically, if you're already in an existing venv, it can be configured to install the package in the current python env.

    poetry config virtualenvs.create false

Install the package and dependencies

    poetry install -vvv

### Importing a dataset

To trigger the dataset importing flow, use `grabds_update_feature_store`

    NAME
        grabds_update_feature_store - Import data into feature store.
        After invoking, data from the given sqlite database and
        extracted features will be imported into the given feature with assignments
        into testing or training sets.
    
    SYNOPSIS
        grabds_update_feature_store DB_PATH <flags>
    
    DESCRIPTION
        Import data into feature store. After invoking, data from the given sqlite database and extracted features will be imported into the given feature with assignments into testing or training sets.
    
    POSITIONAL ARGUMENTS
        DB_PATH
            (str) The path of sqlite3 db file containing raw data
    
    FLAGS
        --store_path=STORE_PATH
            (str) The path of pandas data store (default: feature_pd.pkl)
        --test_ratio=TEST_RATIO
            (float) The ratio of data to be assigned as testing set. The rest is training set. (default: 0.15)
        --features=FEATURES
            (list[str]) The feature(s) to be generated and stored into store (default: [nnlm50, nnlm128])

### Model training

To train a model, use `grabds_train_model`

    NAME
        grabds_train_model - Train a model using dataset from feature store.
        If files at metadata_path and model_path already exists, the script will
    		attempt to load the model from the path and finetune them.
    
    SYNOPSIS
        grabds_train_model METADATA_PATH MODEL_PATH <flags>
    
    DESCRIPTION
        Train a model using dataset from feature store. If files at metadata_path and model_path already exists, the script will attempt to load the model from the path and finetune them.
    
    POSITIONAL ARGUMENTS
        METADATA_PATH
            (str) The metadata yaml to load from and save to.
        MODEL_PATH
            (str) The model path to load from and save to.
    
    FLAGS
        --model_type=MODEL_TYPE
            (str) The model type to train (default: simple_nn)
        --store_path=STORE_PATH
            (str) Path of pandas feature store (default: feature_pd.pkl)
        --val_split=VAL_SPLIT
            (float) Ratio of data for validation during training (default: 0.15)
        --num_epoch=NUM_EPOCH
            (int) Number of epochs to train (default: 50)
        --feature=FEATURE
            (str) The type of feature to as model input (default: nnlm128)
        --feature_size=FEATURE_SIZE
            (int) The size of feature (default: 128)
        --hidden_nodes=HIDDEN_NODES
            (list[int]) # of nodes of each hidden layer (default: [32, 32])

### Packing model

Model server currently takes model in Tensorflow SavedModel format. The model packer `grabds_pack_model` can pack a set of (metadata file, model binary file) files into a serverable SavedModel file.

    NAME
        grabds_pack_model - Create a tf SavedModel combining feature and trained model
    
    SYNOPSIS
        grabds_pack_model METADATAPATH MODELPATH SAVEPATH
    
    DESCRIPTION
        Create a tf SavedModel combining feature and trained model
    
    POSITIONAL ARGUMENTS
        METADATAPATH
            (str) The path of model metadata yaml file.
        MODELPATH
            (str) The path of model weights.
        SAVEPATH
            (str) The path to save the packed tf SavedModel to.